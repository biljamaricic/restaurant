import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppService } from '../app.service';
import { Menu } from '../common.model';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {
  @Input() menu: Menu[]; 
  @Input() menuItem: Menu;
  @Input() index: number = -1;
  @Output() itemToEdit: EventEmitter<Menu> = new EventEmitter();
  @Output() deleteMenuItem: EventEmitter<number> = new EventEmitter();

  constructor(private appService: AppService) {
    this.menu =[];
    this.menuItem = new Menu ({
      id: -1,
      name: '',
      price: 0,
      categoryDTO:{}
    });
  }

  ngOnInit(): void {
    this.getMenu();
  }

  getMenu(){
    this.appService
      .getMenu()
      .subscribe((res: Menu[]) => (this.menu = res));
  }

  delete(id: number){
    this.appService.deleteMenuItem(id).subscribe((res: Menu)=> {this.getMenu()});
  }

  edit(menuItem: Menu) {
    this.itemToEdit.emit(menuItem)
  }
  
}
