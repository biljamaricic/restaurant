import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Menu } from '../common.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public itemToEdit: Menu;

  constructor(private appService: AppService) {
    this.itemToEdit = ({
      id: -1,
      name: '',
      price: 0,
      categoryDTO:{}
    })
  }

  ngOnInit(): void {
  }

  editItem(menuItem: Menu) {
    this.itemToEdit = menuItem;
  }

}
