export class Menu implements MenuInterface {
    public id?: number;
    public name: String;
    public price: number;
    public categoryDTO: CategoryInterface;

    constructor(menuCfg: MenuInterface){
        this.id = menuCfg.id;
        this.name = menuCfg.name;
        this.price = menuCfg.price;
        this.categoryDTO = menuCfg.categoryDTO;
    }
}

interface MenuInterface{
    id?: number;
    name: String;
    price: number;
    categoryDTO: CategoryInterface;
}

export interface CategoryInterface{
    id?: number;
    name?: String;
}