import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { CategoryInterface, Menu } from '../common.model';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.css']
})
export class AddMenuComponent implements OnInit {
  public newMenuItem: Menu;
  public categories: CategoryInterface[] = [];
  @Input() itemToEdit: Menu;

  constructor(private appService: AppService, private router: Router) {
    this.newMenuItem = new Menu ({
      name: '',
      price: 0,
      categoryDTO: {}
    });

    this.itemToEdit = ({
      id: -1,
      name: '',
      price: 0,
      categoryDTO: {}
    })
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.appService.getCategories().subscribe((res: CategoryInterface[]) => {
      this.categories = res;
    })
  }

  addMenuItem() {
    this.appService.addMenuItem(this.newMenuItem).subscribe((res: Menu) => {
      this.newMenuItem = res;
      this.router.navigate(['/menu']);
    });
  }

  resetMenu(){
    this.newMenuItem = {
      name: '',
      price: 0,
      categoryDTO: {}
    }
  }
}
