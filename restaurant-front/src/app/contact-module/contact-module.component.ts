import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-contact-module',
  templateUrl: './contact-module.component.html',
  styleUrls: ['./contact-module.component.css']
})
export class ContactModuleComponent implements OnInit {

  constructor(private appService: AppService) { }

  ngOnInit(): void {
  }

}
