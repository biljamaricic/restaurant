import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ContactModuleComponent } from './contact-module/contact-module.component';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './about/about.component';
import { MenuComponent } from './menu/menu.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { AddMenuComponent } from './add-menu/add-menu.component';
import { FilterMenuComponent } from './filter-menu/filter-menu.component';

const appRoutes: Routes = [
  { path: 'menu/id', component: MenuComponent },
  { path: 'menu', component: MenuListComponent},
  { path: 'contact', component: ContactModuleComponent },
  { path: 'about', component: AboutComponent },
  { path: 'main', component: MainComponent },
  { path: '', redirectTo: 'menu', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ContactModuleComponent,
    MainComponent,
    AboutComponent,
    MenuComponent,
    MenuListComponent,
    AddMenuComponent,
    FilterMenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
