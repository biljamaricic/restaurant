package vp.spring.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.server.converter.CategoryConverter;
import vp.spring.server.dto.CategoryDTO;
import vp.spring.server.model.Category;
import vp.spring.server.service.CategoryService;

@RestController
@RequestMapping(value = "/api/categories")
@CrossOrigin(origins = "http://localhost:4200")
public class CategoryController {

	@Autowired
	CategoryService categoryService;
	
	@Autowired
	CategoryConverter categoryConverter;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity getAll() {
		List<Category> categories = categoryService.findAll();
		
		List<CategoryDTO> retVal = categoryConverter.toDtoList(categories);
		
		return new ResponseEntity(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity getById(@PathVariable Long id) {
		Category category = categoryService.findOne(id);
		
		if(category != null) {
			CategoryDTO dto = new CategoryDTO(category);
			return new ResponseEntity(dto, HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND); 
		}
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity create(@RequestBody CategoryDTO categoryDTO) {
		Category category = new Category();
		category.setId(categoryDTO.getId());
		category.setName(categoryDTO.getName());
		
		category = categoryService.save(category);
		
		return new ResponseEntity(new CategoryDTO(category), HttpStatus.CREATED);
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable Long id, @RequestBody CategoryDTO categoryDTO) {
		Category category = categoryService.findOne(id);
		
		if(category == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
		category.setId(categoryDTO.getId());
		category.setName(categoryDTO.getName());
		category = categoryService.save(category);
		
		return new ResponseEntity(new CategoryDTO(category), HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable Long id) {
		Category category = categoryService.findOne(id);
		
		if(category != null) {
			categoryService.remove(id);
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
}
