package vp.spring.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.server.converter.MenuConverter;
import vp.spring.server.dto.MenuDTO;
import vp.spring.server.model.Category;
import vp.spring.server.model.Menu;
import vp.spring.server.service.MenuService;

@RestController
@RequestMapping(value = "/api/menu")
@CrossOrigin(origins = "http://localhost:4200")
public class MenuController {

	@Autowired
	MenuService menuService;
	
	@Autowired
	MenuConverter menuConverter;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity getMenuPage(@RequestParam(required = false, defaultValue = "") String name, Pageable page) {
		Page<Menu> menu;
		
		if(name != null) {
			menu = menuService.findByNameContains(page, name);
		} else {
			menu = menuService.findAll(page);
		}
		
		List<MenuDTO> retVal = menuConverter.toDtoList(menu.getContent());
		
		return new ResponseEntity(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity getById(@PathVariable Long id) {
		Menu menu = menuService.findOne(id);
		
		if(menu == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity(new MenuDTO(menu), HttpStatus.OK);
		}
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity create(@RequestBody MenuDTO menuDTO) {
		Menu menu = new Menu();
		menu.setId(menuDTO.getId());
		menu.setName(menuDTO.getName());
		menu.setPrice(menuDTO.getPrice());
		
		Category category = new Category();
		category.setId(menuDTO.getCategoryDTO().getId());
		category.setName(menuDTO.getCategoryDTO().getName());
		menu.setCategory(category);
		
		menu = menuService.save(menu);
		
		return new ResponseEntity(new MenuDTO(menu), HttpStatus.CREATED);

	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable Long id, @RequestBody MenuDTO menuDTO) {
		Menu menu = menuService.findOne(id);
		
		if(menu == null) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
		menu.setName(menuDTO.getName());
		menu.setPrice(menuDTO.getPrice());
		
		Category category = new Category();
		category.setId(menuDTO.getCategoryDTO().getId());
		category.setName(menuDTO.getCategoryDTO().getName());
		menu.setCategory(category);
		
		menu = menuService.save(menu);
		return new ResponseEntity(new MenuDTO(menu), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		Menu menu = menuService.findOne(id);
		
		if(menu != null) {
			menuService.remove(id);
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
	
}
