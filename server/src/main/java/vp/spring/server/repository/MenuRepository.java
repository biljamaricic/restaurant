package vp.spring.server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.spring.server.model.Menu;

@Component
public interface MenuRepository extends JpaRepository<Menu, Long> {

	Page<Menu> findByNameContainsIgnoreCase(Pageable page, String name);
}
