package vp.spring.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.spring.server.model.User;

@Component
public interface UserRepository extends JpaRepository<User, Long> {
	public User findByUsername(String username);
}
