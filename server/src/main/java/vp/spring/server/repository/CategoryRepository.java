package vp.spring.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.spring.server.model.Category;

@Component
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
