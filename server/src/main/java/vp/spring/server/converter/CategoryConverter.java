package vp.spring.server.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import vp.spring.server.dto.CategoryDTO;
import vp.spring.server.model.Category;

@Component
public class CategoryConverter {

	public List<CategoryDTO> toDtoList(List<Category> categories) {
		List<CategoryDTO> retVal = new ArrayList();
		
		for(Category category: categories) {
			CategoryDTO dto = new CategoryDTO(category);
			retVal.add(dto);
		}
		return retVal;
	}
}
