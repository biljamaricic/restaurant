package vp.spring.server.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import vp.spring.server.dto.CategoryDTO;
import vp.spring.server.dto.MenuDTO;
import vp.spring.server.model.Menu;

@Component
public class MenuConverter {

	public List<MenuDTO> toDtoList(List<Menu> menus) {
		List<MenuDTO> retVal = new ArrayList();
		for(Menu menu: menus) {
			MenuDTO menuDTO = new MenuDTO(menu);
			CategoryDTO categoryDTO = new CategoryDTO(menu.getCategory());
			menuDTO.setCategoryDTO(categoryDTO);
			retVal.add(menuDTO);
		}
		return retVal;
	}
}
