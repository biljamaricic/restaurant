package vp.spring.server.converter;

import java.util.ArrayList;
import java.util.List;

import vp.spring.server.dto.UserDTO;
import vp.spring.server.model.User;

public class UserConverter {

	public List<UserDTO> toDtoList(List<User> users) {
		List<UserDTO> retVal = new ArrayList();
		for(User user: users) {
			UserDTO dto = new UserDTO(user);
			retVal.add(dto);
		}
		return retVal;
	}
	
}
