package vp.spring.server.dto;



import vp.spring.server.model.Category;
import vp.spring.server.model.Menu;

public class MenuDTO {

	private Long id;
	
	private String name;
	
	private double price;
	
	private CategoryDTO categoryDTO;

	public MenuDTO() {
		super();
	}

	public MenuDTO(Long id, String name, double price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
	public MenuDTO(Menu menu) {
		this.id = menu.getId();
		this.name = menu.getName();
		this.price = menu.getPrice();
		this.categoryDTO = new CategoryDTO(menu.getCategory());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public CategoryDTO getCategoryDTO() {
		return categoryDTO;
	}

	public void setCategoryDTO(CategoryDTO categoryDTO) {
		this.categoryDTO = categoryDTO;
	}
	
	
}
