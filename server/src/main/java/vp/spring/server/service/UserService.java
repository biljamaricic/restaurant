package vp.spring.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.server.model.User;
import vp.spring.server.repository.UserRepository;

@Component
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	   public User findById(Long id){return userRepository.findById(id).get();}

	   public User findByUsername(String username){return userRepository.findByUsername(username);}

	   public User save(User user){
	       return userRepository.save(user);
	    }
}
