package vp.spring.server.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.server.model.Menu;
import vp.spring.server.repository.MenuRepository;

@Component
public class MenuService {

	@Autowired
	MenuRepository menuRepository;
	
	public Page<Menu> findAll(Pageable page) {
		return menuRepository.findAll(page);
	} 
	
	public List<Menu> findALl() {
		return menuRepository.findAll();
	}
	
	public Menu findOne(Long id) {
		return menuRepository.findById(id).get();
	}
	
	public Page<Menu> findByNameContains(Pageable page, String name) {
		return menuRepository.findByNameContainsIgnoreCase(page, name);
	}
	
	public Menu save(Menu menu) {
		return menuRepository.save(menu);
	}
	
	public void remove(Long id) {
		menuRepository.deleteById(id);
	}
}
