package vp.spring.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.server.model.Category;
import vp.spring.server.repository.CategoryRepository;

@Component
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	
	public List<Category> findAll(){
		return categoryRepository.findAll();
	}
	
	public Category findOne(Long id) {
		return categoryRepository.findById(id).get();
	}
	
	public Category save(Category category) {
		return categoryRepository.save(category);
	}
	
	public void remove(Long id) {
		categoryRepository.deleteById(id);
	}
}
